﻿using UnityEngine;
using System.Collections;

/// <summary>
/// 弾クラス
/// </summary>
public class Bullet : MonoBehaviour
{
	public float speed = 1f;    // 移動速度

	/// <summary>
	/// 初期化
	/// </summary>
	private void Start()
	{

	}

	/// <summary>
	/// 更新
	/// </summary>
	private void Update()
	{
		transform.position += transform.right * speed;

		if ((Camera.main.WorldToViewportPoint(transform.position).x < -10f) ||
			(Camera.main.WorldToViewportPoint(transform.position).x > 10f) ||
			(Camera.main.WorldToViewportPoint(transform.position).y < -10f) ||
			(Camera.main.WorldToViewportPoint(transform.position).y > 10f))
		{
			Destroy(gameObject);
		}
    }
}