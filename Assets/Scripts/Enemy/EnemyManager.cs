﻿using UnityEngine;
using System.Collections;

public class EnemyManager : MonoBehaviour
{
	public float delay = 1f;						// 敵を生成する待ち時間
	public GameObject[] enemy = new GameObject[0];	// トゥース(敵)のプレハブ

	/// <summary>
	/// 初期化
	/// </summary>
	void Start()
	{
		// コルーチンをスタートする
		StartCoroutine("SetEnemy");
	}

	/// <summary>
	/// 敵をセットする
	/// </summary>
	/// <returns>?</returns>
	IEnumerator SetEnemy()
	{
		while (true)
		{
			// 生成する座標を設定する
			Vector3 position = Camera.main.ViewportToWorldPoint(new Vector3(1.5f, Random.Range(0f, 1f), -Camera.main.transform.position.z));

			// 敵を生成する
			Instantiate(enemy[0], position, enemy[0].transform.rotation);

			// 待つ
			yield return new WaitForSeconds(delay);
		}
	}
}
