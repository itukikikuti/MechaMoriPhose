﻿using UnityEngine;
using System.Collections;

/// <summary>
/// トゥース(敵)クラス
/// </summary>
public class Tooth : MonoBehaviour
{
	public float speed = 1f;    // 速度
	public bool escape = false;	// 逃げる
	public static float scrollSpeed = 0.1f;	// スクロールするスピード

	/// <summary>
	/// 初期化
	/// </summary>
	private void Start()
	{

	}

	/// <summary>
	/// 更新
	/// </summary>
	private void Update()
	{
		// 画面のだいぶ右に行っていた場合
		if(Camera.main.WorldToViewportPoint(transform.position).x > 2f)
		{
			// 逃げるフラグをfalseにする
			escape = false;
		}

		// 逃げるフラグがtrueだった場合
		if(escape)
		{
			// 右に移動する
			transform.position += new Vector3(speed, 0f);

			return;
		}

		// プレイヤーと離れていた場合
		if (Vector3.Distance(transform.position, GameObject.Find("Player").transform.position) > 10f)
		{
			// スクロールする
			transform.position -= new Vector3(scrollSpeed, 0f);

			return;
		}

		// 画面内に同じ種類の敵が5体いなかった場合
		if (true)
		{
			// 逃げるフラグをtrueにする
			escape = true;
			
			// 弾のプレハブを取得する
			GameObject bullet = Resources.Load<GameObject>("Prefabs/Bullet/ToothBullet");

			// プレイヤーへの角度を計算する
			float angle = Mathf.Atan2(GameObject.Find("Player").transform.position.y - transform.position.y, GameObject.Find("Player").transform.position.x - transform.position.x);

			// プレイヤーのほうを向く
			transform.eulerAngles = new Vector3(0f, 0f, angle / Mathf.PI * 180f);

			// 弾を生成する
			Instantiate(bullet, transform.position, transform.rotation);

			// 角度を戻す
			transform.eulerAngles = new Vector3();

			return;
		}
	}

	/// <summary>
	/// 当たった時の処理
	/// </summary>
	/// <param name="obj">当たったオブジェクト</param>
	private void OnTriggerEnter2D(Collider2D obj)
	{
		// 当たったオブジェクトのタグがBulletだった場合
		if (obj.tag == "PlayerBullet")
		{
			// 自分を消す
			Destroy(gameObject);
		}
	}
}
