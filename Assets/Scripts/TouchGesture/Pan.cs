﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// パンジェスチャークラス
/// </summary>
public class Pan : MonoBehaviour
{
	public event System.Action<Touch> panned = null;   // パンした時に呼び出すイベント

	/// <summary>
	/// 更新
	/// </summary>
	private void Update()
	{
		// タッチしている数だけループする
		for (int i = 0; i < Input.touchCount; ++i)
		{
			// タッチが終わった場合
			if ((Input.touches[i].phase == TouchPhase.Moved) ||
				(Input.touches[i].phase == TouchPhase.Stationary))
			{
				// パンイベントを呼び出す
				panned(Input.touches[i]);
			}
		}
	}
}
