﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// フリックジェスチャークラス
/// </summary>
public class Flick : MonoBehaviour
{
	public event System.Action<Touch> flicked = null;	// フリックした時に呼び出すイベント
	public float speed = 0.5f;  // フリックになる移動量
	private List<Touch> oldTouch = new List<Touch>();	// 前フレームのタッチ

	/// <summary>
	/// 更新
	/// </summary>
	private void Update()
	{
		// タッチしている数だけループする
		for (int i = 0; i < Input.touchCount; ++i)
		{
			// タッチが終わった場合
			if ((Input.touches[i].phase == TouchPhase.Ended) ||
				(Input.touches[i].phase == TouchPhase.Canceled))
			{
				// 前フレームの移動量がフリックになる移動量より大きかった場合
				if (((oldTouch[i].deltaPosition.x < -speed) || (oldTouch[i].deltaPosition.x > speed) ||
					(oldTouch[i].deltaPosition.y < -speed) || (oldTouch[i].deltaPosition.y > speed)))
				{
					// フリックイベントを呼び出す
					flicked(oldTouch[i]);
				}

				// 要素を削除する
				oldTouch.Remove(oldTouch[i]);
            }
			else
			{
				// 要素が足りない場合
				if(oldTouch.Count < Input.touchCount)
				{
					// 新しい要素を追加する
					oldTouch.Add(new Touch());
				}

				// 移動量を保存する
				oldTouch[i] = Input.touches[i];
            }
		}
	}
}
