﻿using UnityEngine;
using System.Collections;

/// <summary>
/// プレイヤークラス
/// </summary>
public class Player : MonoBehaviour
{
	public float speed = 0f;						// 速度
	private Vector3 targetPosition = new Vector3(); // 目標の座標

	/// <summary>
	/// 初期化
	/// </summary>
	private void Start()
	{
		// 目標の座標をプレイヤーの座標にする
		targetPosition = transform.position;

		// パンイベントを登録する
		GetComponent<Pan>().panned += Move;

		// フリックイベントを登録する
		GetComponent<Flick>().flicked += Shoot;
    }

	/// <summary>
	/// 移動処理
	/// </summary>
	private void Move(Touch touch)
	{
		// パンしている座標を目標の座標にする
		targetPosition = Camera.main.ScreenToWorldPoint(new Vector3(touch.position.x, touch.position.y, -Camera.main.transform.position.z));

		// 目標の座標とプレイヤーの座標が離れていた場合
		if (Vector3.Distance(targetPosition, transform.position) > speed)
		{
			// 進む角度を計算する
			float angle = Mathf.Atan2(targetPosition.y - transform.position.y, targetPosition.x - transform.position.x);

			// プレイヤーを進む角度に動かす
			transform.position += new Vector3(Mathf.Cos(angle) * speed, Mathf.Sin(angle) * speed);
		}
	}

	/// <summary>
	/// 発射処理
	/// </summary>
	private void Shoot(Touch touch)
	{
		// 弾を発射する角度を計算する
		float angle = Mathf.Atan2(touch.deltaPosition.y, touch.deltaPosition.x);

		// 弾を発射する方向を向く
		transform.eulerAngles = new Vector3(0f, 0f, angle / Mathf.PI * 180f);

		// 弾のプレハブを取得する
		GameObject bullet = Resources.Load<GameObject>("Prefabs/Bullet/PlayerBullet");

		// 弾を生成する
		Instantiate(bullet, transform.position, transform.rotation);

		// 元の方向を向く
		transform.eulerAngles = new Vector3(0f, 0f, 0f);
	}
}
